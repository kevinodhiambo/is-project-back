"""IsProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

# Routes
from Inventory.views import InventoryView
from Products.views import ProductsView
from Shops.views import ShopsView
from Sales.views import SalesView, saleReports
from Users.views import UsersView, loggedInUser

router = routers.DefaultRouter()
router.register('shops', ShopsView)
router.register('products', ProductsView)
router.register('inventory', InventoryView)
router.register('sales', SalesView)
router.register('users', UsersView)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('logged-user/', loggedInUser),
    path('sale-reports/', saleReports),
    path('api/auth/', include('rest_framework.urls')),
    path('api/token/', TokenObtainPairView.as_view()),
    path('api/token/refresh/', TokenRefreshView.as_view()),

]
