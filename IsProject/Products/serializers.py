from rest_framework import serializers
from .models import Products

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ('id', 'shop_id', 'name', 'measurement', 'description', 'created_at', 'updated_at')