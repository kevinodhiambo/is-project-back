import uuid

from django.db import models
from Shops.models import Shops

class Products(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    shop_id = models.ForeignKey(Shops, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    measurement = models.CharField(max_length=100, null=True)
    description = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)