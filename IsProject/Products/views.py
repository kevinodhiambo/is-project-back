from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework.response import Response
from django.contrib.auth.models import User

from Shops.models import Shops
from .models import Products
from .serializers import ProductSerializer

class ProductsView(viewsets.ModelViewSet):

    def list(self, request):
        shop = Shops.objects.filter(user_id = request.user.id).get()
        queryset = Products.objects.filter(shop_id=shop.id)
        serializer = ProductSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        shop = Shops.objects.filter(user_id=request.user.id).get()

        data = request.data
        product_data = {
            'shop_id': shop.id,
            'name': data['name'],
            'description': data['description'],
        }
        product = ProductSerializer(data=product_data)
        if product.is_valid():
            product.save()

        return JsonResponse({'message': 'Added Successfully'})

    queryset = Products.objects.all()
    serializer_class = ProductSerializer