import json

from django.shortcuts import render
from django.http import JsonResponse
from rest_framework import viewsets
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import Token

from Shops.models import Shops
from Shops.serializers import ShopSerializer
from .serializers import UserSerializer, UserNoPasswordSerializer
from django.contrib.auth.hashers import make_password

# Create your views here.
class UsersView(viewsets.ModelViewSet):
    def create(self, request):
        data = request.data
        data['username'] = data['email']
        data['name'] = data['name']
        data['town'] = data['town']

        user_data = {
            'username': data['email'],
            'password': make_password(data['password']),
            'first_name': data['first_name'],
            'last_name': data['last_name'],
            'email': data['email'],
        }
        user_serializer = UserSerializer(data=user_data)

        if not user_serializer.is_valid():
            return JsonResponse(user_serializer.errors, status=412)

        shop_data = {
            'user_id': 1,
            'name': data['name'],
            'town': data['town']
        }

        shop_serializer = ShopSerializer(data=shop_data)

        if not shop_serializer.is_valid():
            return JsonResponse(shop_serializer.errors, status=412)

        user = user_serializer.save()

        shop_data = {
            'user_id': user.id,
            'name': data['name'],
            'town': data['town']
        }

        shop_serializer = ShopSerializer(data=shop_data)
        shop_serializer.is_valid()
        shop = shop_serializer.save()

        serialized_user = UserSerializer(user, many=False).data
        serialized_shop = ShopSerializer(shop, many=False).data
        return JsonResponse(
            {
                'message': 'Added Successful',
                'user': serialized_user,
                'shop': serialized_shop
            }
        )
    serializer_class = UserSerializer
    queryset = User.objects.all()


@api_view(['GET'])
def loggedInUser(request):

    user = User.objects.filter(pk=request.user.id).get()
    serialized_user = UserSerializer(user, many=False).data

    shop = Shops.objects.filter(user_id=serialized_user['id']).get()
    serialized_shop = ShopSerializer(shop, many=False).data
    print(shop)

    return JsonResponse(
        {'user': serialized_user,
        'shop': serialized_shop}
    )

@api_view(['PATCH'])
def updateProfile(request):
    data = request.data

    user = User.objects.filter(pk=request.user.id).get()
    user.first_name = data['first_name']
    user.last_name = data['last_name']
    user.email = data['email']
    user.save()
    # serializer = UserNoPasswordSerializer(data=data)

    return JsonResponse({'message': 'Update Successful'})

@api_view(['PATCH'])
def updatePassword(request):
    data = request.data

    user = User.objects.filter(pk=request.user.id).get()
    user.password = data['password']
    user.save()
    # serializer = UserNoPasswordSerializer(data=data)


    return JsonResponse({'message': 'Update Successful'})