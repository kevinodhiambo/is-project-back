from rest_framework import serializers
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        proxy = True
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'password', 'last_login', 'date_joined')

class UserNoPasswordSerializer(serializers.ModelSerializer):
    class Meta:
        proxy = True
        model = User
        fields = (
        'id', 'username', 'first_name', 'last_name', 'email', 'last_login',
        'date_joined')