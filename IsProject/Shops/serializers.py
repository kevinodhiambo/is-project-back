from rest_framework import serializers
from .models import Shops

class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shops
        fields = ('id', 'user_id', 'name', 'town', 'created_at', 'updated_at')