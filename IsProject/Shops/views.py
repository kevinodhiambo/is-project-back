from rest_framework import viewsets
from .models import Shops
from .serializers import ShopSerializer

class ShopsView(viewsets.ModelViewSet):
    queryset = Shops.objects.all()
    serializer_class = ShopSerializer