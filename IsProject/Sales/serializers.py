from rest_framework import serializers
from .models import Sales

class SalesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sales
        fields = ('id', 'shop_id', 'product_id', 'quantity', 'price', 'free_items', 'created_at', 'updated_at')

class SalesReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sales
        fields = ('id', 'shop_id', 'product_id', 'quantity', 'price', 'free_items', 'created_at', 'updated_at')
        depth = 1