from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from Shops.models import Shops
from .models import Sales
from .serializers import SalesSerializer, SalesReadSerializer

class SalesView(viewsets.ModelViewSet):
    def create(self, request):
        data = request.data
        shop = Shops.objects.filter(user_id=request.user.id).get()
        product_data = {
            'shop_id': shop.id,
            'product_id': data['product_id'],
            'quantity': data['quantity'],
            'price': data['price'],
            'free_items': data['free_items'],
        }
        sale = SalesSerializer(data=product_data)
        if sale.is_valid():
            sale.save()

        return JsonResponse({'message': 'Sale Recorded Successfully'})

    def list(self, request):
        shop = Shops.objects.filter(user_id=request.user.id).get()
        queryset = Sales.objects.filter(shop_id=shop.id)
        sale_serializer = SalesReadSerializer(queryset, many=True).data



        return Response(sale_serializer)


    queryset = Sales.objects.all()
    serializer_class = SalesSerializer


@api_view(['GET'])
def saleReports(request):
    shop = Shops.objects.filter(user_id=request.user.id).get()

    return JsonResponse(
        {'data': ''}
    )