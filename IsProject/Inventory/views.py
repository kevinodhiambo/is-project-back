from rest_framework import viewsets
from rest_framework.response import Response

from Products.models import Products
from Products.serializers import ProductSerializer
from Sales.models import Sales
from Sales.serializers import SalesSerializer
from Shops.models import Shops
from .models import Inventory
from .serializers import InventorySerializer

class InventoryView(viewsets.ModelViewSet):
    def list(self, request):
        shop = Shops.objects.filter(user_id = request.user.id).get()

        products = Products.objects.filter(shop_id=shop.id)
        products_serializer = ProductSerializer(products, many=True).data


        all_inventory = []
        for product in products_serializer:
            inventory = Inventory.objects.filter(product_id=product['id'])
            inventory_serializer = InventorySerializer(inventory, many=True).data

            total = 0
            for inventory_item in inventory_serializer:
                total = total + inventory_item['quantity']

            sales = Sales.objects.filter(shop_id=shop.id, product_id=product['id'])
            sale_serializer = SalesSerializer(sales, many=True).data

            for sale in sale_serializer:
                total = total - sale['quantity'] - sale['free_items']

            data = {
                'name': product['name'],
                'quantity': total
            }

            if data['quantity'] > 0:
                all_inventory.append(data)



        return Response(all_inventory)

    queryset = Inventory.objects.all()
    serializer_class = InventorySerializer